let collection = [];

// Write the queue functions below.

	// Print (output all the elements of the queue)
function print(queue) {
	if (queue != null) {
		for(i=0; i<queue.toArray().length; i++) {
			collection[i]=queue[i];
		}
	} 
	else {
		collection = [];
	}
	return collection;
}

	// Enqueue (add element to rear of queue)
function enqueue(newEntry) {
	if (newEntry != null) {
		collection[collection.length] = newEntry
	} 
	return collection;
}

	// Dequeue (remove element at front of queue)
function dequeue() {
	let newCollection = []
	for(i=1; i<collection.length; i++) {
		newCollection[i-1]=collection[i];
		collection = newCollection;
	}
	return collection;
}

	// Front (show element at the front)
function front() {
	return collection[0];
}
	// Size (show total number of elements)

function size() {
	return collection.length;
}
	// isEmpty (outputs Boolean value describing whether queue is empty or not)
function isEmpty() {
	if (collection != null && collection.length > 0) {
			isEmpty = false;
	} 
	else {
		isEmpty = true;
	}
	return isEmpty;
}

module.exports = {
	// collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};